<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 12/10/18
 * Time: 18:48
 */
$items = [
    [
        "id" => 1,
        "name" => "John",
        "age" => 23,
        "gender" => "M",
    ],
    [
        "id" => 2,
        "name" => "Jane",
        "age" => 21,
        "gender" => "F",
    ]
];

?>
<article>
    <?php
        foreach ($items as $item){
            include __DIR__ . '/../item.view.php';
        }
    ?>
</article>
