<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 12/10/18
 * Time: 20:23
 */
    $routerActive;
    $routes = [
        [
            'key' => 'home',
            'path' => './views/home/home.view.php',
            'label' => 'Home'
        ],
        [
            'key' => 'search',
            'path' => './views/search-content.view.php',
            'label' => 'Search'
        ],
        [
            'key' => 'contact',
            'label' => 'Contact',
            'children' => [
                [
                    'key' => 'sns',
                    'path' => './views/contact/sns.view.php',
                    'label' => 'SNS',
                ],
                [
                    'key' => 'location',
                    'label' => 'Location'
                ]
            ]
        ],
    ];

    function route (string $view) {
        global $routerActive;
        switch ($view) {
            case 'home':
                $routerActive = 'home';
                break;
            case 'search':
                $routerActive = 'search';
                break;
            case 'sns':
                $routerActive = 'sns';
                break;
        }
    }

    function renderRoutes() {
        global $routerActive;
        global $routes;
        return join("", array_map(
            function ($route) use ($routes, $routerActive) {
                return "<li>" . renderNavItem($route, $routes, $routerActive) . "</li>";
            },
            $routes
        ));
    };

    function renderNavItem($route, $routes, $current) {
        if(isset($route['children'])) {
            return '<span class="nav-menu-item'
                    . (isRouteActive($route) ? ' current': '')
                    .'">'
                    . $route['label']
                    . '<div class="nav-sub-menu">'
                    . join("", array_map(function($route) use ($routes, $current){
                        return renderNavItem($route, $routes, $current);
                    }, $route['children']))
                    . '</div>'
                    . '</span>';
        }
        return '<form class="nav-menu-item'
                . (isRouteActive($route) ? ' current': '')
                .'"'
                . 'action="' . $_SERVER['PHP_SELF']
                . '" method="get">'
                . '<input type="hidden" name="view" value="' . $route['key'] . '"/>'
                . '<button type="submit">' . $route['label']  .'</button>'
                . '</form>';
    }

    function isRouteActive($route): bool {
        global $routerActive;
        if (isset($route['children'])) {
            foreach ($route['children'] as $child) {
                $result = isRouteActive($child);
                if ($result) {
                    return true;
                }
            }
        }
        else if ($route['key'] === $routerActive) {
            return true;
        }
        return false;
    }

    function getRoutePath($routes) {
        global $routerActive;

        foreach ($routes as $route) {
            if (isset($route['children'])) {
                $result = getRoutePath($route['children']);
                if ($result !== '') {
                    return $result;
                }
            } else if ($route['key'] === $routerActive) {
                return $route['path'];
            }
        }
    }
    return '';
?>