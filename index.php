<?php
require_once './router.php';
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $view = $_GET['view'] ?? '';

    route($view);
}
?>

<!DOCTYPE html>
<html lang="en-EN">
    <head>
        <meta charset="utf-8"/>

        <link rel="stylesheet" type="text/css" href="./css/header.css"/>
        <link rel="stylesheet" type="text/css" href="./css/sns.css"/>
        <link rel="stylesheet" type="text/css" href="./css/styles.css"/>
        <title></title>
    </head>
    <body>
        <?php include './partials/header.part.php'?>
        <main>
            <?php if (isset($routerActive)) { include getRoutePath($routes); }?>
        </main>
    </body>
</html>

