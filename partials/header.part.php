<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 12/10/18
 * Time: 16:40
 */
    require_once __DIR__ . '/../router.php'
?>

<header>
    <h1>Page title</h1>
    <nav>
        <ul>
            <?= renderRoutes() ?>
            <!--<li><a href="#">Menu 1</a></li>
            <li><a href="#">Menu 2</a></li>
            <li><a href="#">Menu 3</a></li>-->
        </ul>
    </nav>
</header>
